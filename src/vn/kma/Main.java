package vn.kma;

import vn.kma.run.RunC;
import vn.kma.run.RunJava;
import vn.kma.util.TypeLanguage;

public class Main {

    public static void main(String[] args) {
        TypeLanguage t = TypeLanguage.valueOf("JAVA");
        switch (t){
            case C: new RunC("demo");
            case JAVA: new RunJava("Demo");
        }
    }
}
