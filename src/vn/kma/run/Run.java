package vn.kma.run;

public abstract class Run {
    protected String filename;
    protected String outputLocation;
    protected String inputLocation;

    public Run(String filename) {
        this.filename = filename;
    }

    public Run(String filename, String outputLocation, String inputLocation) {
        this.filename = filename;
        this.outputLocation = outputLocation;
        this.inputLocation = inputLocation;
    }

    public Run() {
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getOutputLocation() {
        return outputLocation;
    }

    public void setOutputLocation(String outputLocation) {
        this.outputLocation = outputLocation;
    }

    public String getInputLocation() {
        return inputLocation;
    }

    public void setInputLocation(String inputLocation) {
        this.inputLocation = inputLocation;
    }

    abstract void build();

    abstract void run();
}
