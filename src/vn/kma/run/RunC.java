package vn.kma.run;

import vn.kma.util.Config;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Paths;

public class RunC extends Run {

    public RunC(String filename) {
        super(filename);
        this.inputLocation = Paths.get(Config.ABSOLUTE_PATH_LANGUAGE_CODE,"c",filename+".c").toString();
        this.outputLocation = Paths.get(Config.ABSOLUTE_PATH_LANGUAGE_CODE,"c","exe",filename+".exe").toString();
        build();
        run();
    }

    @Override
    void build() {
        try {
            String command = "gcc -o "+outputLocation+" "+inputLocation;
            Process p = Runtime.getRuntime().exec(command);
            p.waitFor();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    @Override
    void run() {
        try {
            Process p = Runtime.getRuntime().exec(outputLocation);
            BufferedReader stout = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line = null;
            while ((line = stout.readLine())!=null){
                System.out.println(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
