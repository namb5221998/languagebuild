package vn.kma.run;

import vn.kma.util.Config;

import java.io.*;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

public class RunJava extends Run {

    public RunJava(String filename) {
        super(filename);
        this.inputLocation = Paths.get(Config.ABSOLUTE_PATH_LANGUAGE_CODE, "java", filename + ".java").toString();
        this.outputLocation = Paths.get(Config.ABSOLUTE_PATH_LANGUAGE_CODE, "java", "class").toString();
        build();
        run();
    }

    @Override
    void build() {
        try {
            String command = "javac -d " + outputLocation + " " + inputLocation;
            Process p = Runtime.getRuntime().exec(command);
            p.waitFor();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    void run() {
        try {
            String command = "java -cp " + outputLocation + "; " + filename;
            //Param on win
            List<String> params = Arrays.asList("cmd.exe","/c",command);
            ProcessBuilder pb = new ProcessBuilder();
            pb.redirectInput(new File("output.txt"));
            pb.command(params);
            Process p = pb.start();
            BufferedReader stout = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line = null;
            while ((line = stout.readLine())!=null){
                System.out.println(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
